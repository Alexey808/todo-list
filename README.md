# TODO-LIST

**Стэк:**  
 - angular
 - express
 - mongodb
 - docker

**Запуск:**  
  1) `docker compose up -d`  
  2) `localhost:3000`  

**Deploy**  
  - добавить тег `deploy` и запушить https://gitlab.com/Alexey808/todo-list  

# Инфроструктура  
Требуется предварительная настройка на сервере, настройка домена, создание раннера. Ключевые вырезки ниже.  

1) `docker build . -t server`  
2) `docker network create nginx-network`  
3) `ocker run -d -p 80:80 --name server --network nginx-network --rm server`  


## Базовые конфиги  

**NGINX**  
```Dockerfile
FROM nginx:latest
COPY ./default.conf /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
```
```conf
server {
    listen  80;
    server_name  DOMEN.ru www.DOMEN.ru;

    location / {
        proxy_pass http://todo-list-backend-1:3000;
	      proxy_redirect off;
        proxy_intercept_errors on;
    }
}
```

**gitlab-runner**  
> Запуск контейнера:  
```
docker volume create gitlab-runner-config

docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
```
> Регистрация раннера:  
```
docker exec -it gitlab-runner gitlab-runner register \
--non-interactive \
--url "https://gitlab.com" \
--registration-token "RUNNER_TOKEN" \
--executor "docker" \
--docker-image docker:latest \
--description "my description" \
--tag-list "TRIGGER_TAG"
```
> Настройка раннера */etc/gitlab-runner/config.toml*    
```
[[runners]]
  name = "RUNNER_NAME"
  url = "https://gitlab.com"
  token = "RUNNER_TOKEN"
  executor = "docker"
  [runners.docker]
    image = "docker:latest"
    privileged = true
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
```
